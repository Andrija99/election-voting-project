FROM python:3

WORKDIR /app

COPY election/requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

COPY election/daemon.py ./daemon.py
COPY election/configuration.py ./configuration.py
COPY election/models.py ./models.py
COPY election/roleCheck.py ./roleCheck.py

ENTRYPOINT ["python", "/app/daemon.py"]