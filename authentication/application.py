from flask import Flask, request, Response, jsonify

from roleCheck import roleCheck
from configuration import Configuration
from models import database, User, UserRole, Role
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, create_refresh_token, get_jwt, get_jwt_identity
from sqlalchemy import and_
import re

application = Flask(__name__)
application.config.from_object(Configuration)


@application.route("/", methods=["GET"])
def index():
    return "Hello World"


# jmbg provera
def proveraJMBG(jmbg):
    if len(jmbg) == 13:
        m = 0
        DD = jmbg[0] + jmbg[1]
        DD = int(DD)

        MM = jmbg[2] + jmbg[3]
        MM = int(MM)

        YYY = jmbg[4] + jmbg[5] + jmbg[6]
        YYY = int(YYY)

        RR = jmbg[7] + jmbg[8]
        RR = int(RR)

        if (DD < 1 or DD > 31 or MM < 1 or MM > 12 or RR < 70 or RR > 99):
            return False

        for i in range(6):
            m += (7 - i) * (int(jmbg[i]) + int(jmbg[6 + i]))
            ostatak = m % 11
            final = 11 - ostatak

        if (ostatak == 1) or (ostatak == 0 and (int(jmbg[12]) != 0)) or (final != int(jmbg[12])):
            return False

        return True
    else:
        return False


@application.route("/register", methods=["POST"])
def register():
    jmbg = request.json.get("jmbg", "")
    forename = request.json.get("forename", "")
    surname = request.json.get("surname", "")
    email = request.json.get("email", "")
    password = request.json.get("password", "")

    jmbgEmpty = len(jmbg) == 0
    forenameEmpty = len(forename) == 0
    surnameEmpty = len(surname) == 0
    emailEmpty = len(email) == 0
    passwordEmpty = len(password) == 0

    # provera prazno polje
    if (jmbgEmpty):
        return jsonify(message='Field jmbg is missing.'), 400
    if (forenameEmpty):
        return jsonify(message='Field forename is missing.'), 400
    if (surnameEmpty):
        return jsonify(message='Field surname is missing.'), 400
    if (emailEmpty):
        return jsonify(message='Field email is missing.'), 400
    if (passwordEmpty):
        return jsonify(message='Field password is missing.'), 400

    proveraJMBGBool = proveraJMBG(jmbg)

    if not proveraJMBGBool:
        return jsonify(message='Invalid jmbg.'), 400

    emailProvera = re.match(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b', email)
    if not emailProvera:
        return jsonify(message='Invalid email.'), 400

    passwordProvera = re.match(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$", password)
    if not passwordProvera:
        return jsonify(message='Invalid password.'), 400

    emailPostoji = User.query.filter(User.email == email).first()

    application.logger.debug(emailPostoji)
    if emailPostoji:
        return jsonify(message='Email already exists.'), 400

    user = User(jmbg=jmbg, forename=forename, surname=surname, email=email, password=password)

    database.session.add(user)
    database.session.commit()

    userRole = UserRole(userId=user.id, roleId=2)

    database.session.add(userRole)
    database.session.commit()

    return Response("Registration successful", status=200)


jwt = JWTManager(application)


@application.route("/login", methods=["POST"])
def login():
    email = request.json.get ('email', '')
    password = request.json.get ('password', '')
    emailEmpty = len(email) == 0
    passwordEmpty = len(password) == 0

    if (emailEmpty):
        return jsonify(message='Field email is missing.'), 400
    if (passwordEmpty):
        return jsonify(message='Field password is missing.'), 400

    emailProvera = re.match(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b', email)
    if not emailProvera:
        return jsonify(message='Invalid email.'), 400


    user = User.query.filter(and_(User.email == email, User.password == password)).first()
    if (not user):
        return jsonify(message="Invalid credentials."), 400

    additionalClaims = {
        'forename': str(user.forename),
        'surname': str(user.surname),
        'jmbg': str(user.jmbg),
        "email": str(user.email),
        'roles': [str(role) for role in user.roles]
    }

    accessToken = create_access_token(identity=user.email, additional_claims=additionalClaims)
    refreshToken = create_refresh_token(identity=user.email, additional_claims=additionalClaims)

    return jsonify(accessToken=accessToken, refreshToken=refreshToken), 200

@application.route("/refresh", methods=["POST"])
@jwt_required( refresh = True)
def refresh():
    identity = get_jwt_identity()
    refrClaims = get_jwt()

    additionalClaims = {
        "jmbg": refrClaims["jmbg"],
        "forename": refrClaims["forename"],
        "email": refrClaims["email"],
        "surname": refrClaims["surname"],
        "roles": refrClaims["roles"]
    }

    return jsonify (accessToken = create_access_token(identity= identity, additional_claims=additionalClaims))

@application.route("/delete", methods=["POST"])
@roleCheck (role = "admin")
def delete():
    email = request.json.get('email')

    if not email:
        return jsonify(message='Field email is missing.'), 400

    email_valid = re.search("^.+@.+\..{2,}$", email)
    # emailProvera = re.match(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b', email)
    if not email_valid:
        return jsonify(message='Invalid email.'), 400

    user = User.query.filter(User.email == email).first()

    if (not user):
        return jsonify(message= "Unknown user."), 400

    userrole=UserRole.query.filter(UserRole.userId == user.id).first()

    database.session.delete(userrole)
    database.session.commit()

    database.session.delete(user)
    database.session.commit()

    return Response (status=200)


if (__name__ == "__main__"):
    database.init_app(application)
    application.run(debug=True, host="0.0.0.0", port=5000)
