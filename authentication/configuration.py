import os;
from datetime import timedelta

databaseURL = os.environ["DATABASE_URL"];
dbpass = os.environ["MYSQL_ROOT_PASSWORD"]

class Configuration ( ):
    SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://root:{dbpass}@{databaseURL}/authentication";
    JWT_SECRET_KEY = "JWT_SECRET_KEY"
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=30)
