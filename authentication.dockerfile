FROM python:3

WORKDIR /app

COPY authentication/requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

COPY authentication/application.py ./application.py
COPY authentication/configuration.py ./configuration.py
COPY authentication/models.py ./models.py
COPY authentication/roleCheck.py ./roleCheck.py

ENTRYPOINT ["python", "/app/application.py"]

