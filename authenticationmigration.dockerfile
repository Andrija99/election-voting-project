FROM python:3

WORKDIR /app

COPY authentication/requirements.txt ./requirements.txt

RUN pip install -r ./requirements.txt

COPY authentication/migrate.py ./migrate.py
COPY authentication/configuration.py ./configuration.py
COPY authentication/models.py ./models.py

ENTRYPOINT ["python", "/app/migrate.py"]
