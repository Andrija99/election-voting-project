FROM python:3

WORKDIR /app

COPY election/requirements.txt ./requirements.txt

RUN pip install -r ./requirements.txt

COPY election/migrate.py ./migrate.py
COPY election/configuration.py ./configuration.py
COPY election/models.py ./models.py

ENTRYPOINT ["python", "/app/migrate.py"]
