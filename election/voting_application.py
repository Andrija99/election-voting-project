from flask import Flask, request, Response, jsonify
from roleCheck import roleCheck
from configuration import Configuration
from models import database, Participant, Election, ElectionParticipant
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, create_refresh_token, get_jwt, get_jwt_identity
from redis import Redis
import csv
import io

application = Flask (__name__)
application.config.from_object(Configuration)
jwt = JWTManager(application)

@application.route("/", methods=["GET"])
def proba():
    return "radi radi radi"

@application.route("/vote", methods=["POST"])
@roleCheck(role="user")
def vote():
    try:
        content = request.files["file"].stream.read().decode("utf-8");
    except:
        return jsonify(message="Field file is missing."), 400

    stream = io.StringIO(content);
    reader = csv.reader(stream);

    cnt = 0
    for row in reader:

        if (len(row) != 2):
            return jsonify(message="Incorrect number of values on line " + str(cnt) + "."), 400

        guid = row[0]
        pollNumber = row[1]

        if (int(pollNumber) <= 0):
            return jsonify(message="Incorrect poll number on line " + str(cnt) + "."), 400

        cnt += 1

    claims = get_jwt();
    stream = io.StringIO(content);
    reader = csv.reader(stream)

    with Redis(host='redis', port=6379) as redis:
        for row in reader:
            guid = row[0]
            pollNumber = row[1]

            line = str(guid + "," + pollNumber + "," + claims["jmbg"])

            redis.rpush("votes", line);

    return Response(status=200)



if (__name__ == "__main__"):

    database.init_app(application)
    application.run(debug=True, host="0.0.0.0", port=5002)