from flask import Flask, request, Response, jsonify

from configuration import Configuration
from models import database, Participant, Election, ElectionParticipant, InvalidVote
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, create_refresh_token, get_jwt, get_jwt_identity
import isodate.isodatetime;
from datetime import datetime, date
import datetime
from sqlalchemy import and_, distinct,join
import re

application = Flask(__name__)
application.config.from_object(Configuration)

@application.route("/", methods=["GET"])
def nista():
    return 'shduafjanfk'

@application.route('/modifikacija', methods=['POST'])
def modifikacija():
    ime = request.json.get("ime")
    search = "%{}%".format(ime)

    election = Election.query.join(ElectionParticipant, Election.id == ElectionParticipant.idElection).\
        join(Participant, ElectionParticipant.idParticipant == Participant.id).filter(Participant.name.like(search))\
        .all()

    lista = []

    for el in election:
        ispis = {
            "start" : el.start,
            "end" : el.end,
            "individual" : el.individual
        }
        lista.append(ispis)

    return jsonify(modifikacija=lista), 200



if (__name__ == "__main__"):

    database.init_app(application)
    application.run(debug=True, host="0.0.0.0", port=5005)