import os;

databaseURL = os.environ["DATABASE_URL"];
dbpass = os.environ["MYSQL_ROOT_PASSWORD"]

class Configuration ( ):
    SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://root:{dbpass}@{databaseURL}/election";
    JWT_SECRET_KEY = "JWT_SECRET_KEY"
    REDIS_HOST = "redis"

