from datetime import datetime
from operator import and_

from flask import Flask, request, Response, jsonify
from roleCheck import roleCheck
from configuration import Configuration
from models import database, Participant, Election, Vote, ElectionParticipant, InvalidVote
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, create_refresh_token, get_jwt, get_jwt_identity
from redis import Redis
import pytz

application = Flask ( __name__ );
application.config.from_object ( Configuration );


database.init_app ( application );

with application.app_context ( ) as context:

    with Redis(host="redis", port=6379) as redis:
        while (1):
            vote = redis.lpop("votes")
            if (vote != None):

                elections = Election.query.all()
                active = False
                id = 0

                for election in elections:

                    tz = pytz.timezone('Europe/Vienna')
                    now = datetime.now(tz=tz)

                    if (election.start.replace(tzinfo=None) < now.replace(tzinfo=None) and election.end.replace(
                            tzinfo=None) > now.replace(tzinfo=None)):
                        active = True
                        id = election.id
                        break

                if (active == True):

                    vote=str(vote)
                    vote = vote.split(",")
                    guid = str(vote[0]).split("'")[1]
                    poll = int(vote[1])
                    jmbg = str(vote[2]).split("'")[0]

                    list = Vote.query.filter(and_(Vote.id_paper == guid, Vote.id_election == id)).first()
                    participant_election = ElectionParticipant.query.filter(
                        and_(ElectionParticipant.idElection == id, ElectionParticipant.pollNumbers == poll)).first()

                    if (list):
                        invalid_vote = InvalidVote(electionOfficialJMBG=jmbg, ballotGuid=guid, pollnumber=poll,
                                                    reason="Duplicate ballot.", electionId=id)
                        database.session.add(invalid_vote)
                        database.session.commit()
                    elif (not participant_election):
                        invalid_vote = InvalidVote(electionOfficialJMBG=jmbg, ballotGuid=guid, pollnumber=poll,
                                                    reason="Invalid poll number.", electionId=id)
                        database.session.add(invalid_vote)
                        database.session.commit()
                    else:

                        participant_election.number_votes += 1
                        database.session.commit()
                        voter = Vote(id_paper=guid, id_election=id)

                        database.session.add(voter)
                        database.session.commit()




