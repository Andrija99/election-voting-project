from flask_sqlalchemy import SQLAlchemy

database = SQLAlchemy ()

class Participant (database.Model):

    __tablename__ = "participant"

    id = database.Column(database.Integer, primary_key=True)
    name = database.Column(database.String(256), nullable=False)
    individual = database.Column(database.Boolean, nullable = False)

class Election (database.Model):

    __tablename__ = "election"

    id = database.Column(database.Integer, primary_key=True)
    start = database.Column(database.DateTime, nullable=False)
    end = database.Column(database.DateTime, nullable=False)
    individual = database.Column(database.Boolean, nullable=False)

class ElectionParticipant (database.Model):

    __tablename__ = "electionparticipant"

    id = database.Column(database.Integer, primary_key=True)
    idParticipant = database.Column(database.Integer, nullable=False)
    idElection = database.Column(database.Integer, nullable=False)
    pollNumbers = database.Column(database.Integer, nullable=False)
    number_votes=database.Column(database.Integer, nullable=False );


class InvalidVote(database.Model):
    __tablename__ = "invalidvote";

    id = database.Column(database.Integer, primary_key=True);
    electionOfficialJMBG = database.Column(database.String(13), nullable=False);
    ballotGuid = database.Column(database.String(256), nullable=False);
    pollnumber = database.Column(database.Integer, nullable=False );
    reason= database.Column(database.String(256), nullable=False);
    electionId = database.Column(database.Integer, nullable=False);

class Vote(database.Model):
    __tablename__ = "vote";

    id_paper=database.Column(database.String(256), primary_key=True);
    id_election=database.Column(database.Integer, primary_key=True);