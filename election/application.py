from flask import Flask, request, Response, jsonify

from roleCheck import roleCheck
from configuration import Configuration
from models import database, Participant, Election, ElectionParticipant, InvalidVote
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, create_refresh_token, get_jwt, get_jwt_identity
import isodate.isodatetime;
from datetime import datetime, date
import datetime
from sqlalchemy import and_
import re

application = Flask(__name__)
application.config.from_object(Configuration)

@application.route("/", methods=["GET"])
def index():
    return "WAASSSUPP BOIIII"

jwt = JWTManager(application)

@application.route("/createParticipant", methods=["POST"])
@roleCheck(role= "admin")
def createParticipant():

    name = request.json.get("name", "")
    individual = request.json.get("individual", "")

    if len(name) == 0 :
        return jsonify(message="Field name is missing."), 400

    if individual is None:
        return jsonify(message = "Field individual is missing."), 400

    if type(individual) != bool:
        return jsonify(message="Field individual is missing."), 400

    participant = Participant(name = name, individual = individual)

    database.session.add(participant)
    database.session.commit()

    return jsonify(id = participant.id), 200


@application.route("/getParticipants", methods=["GET"])
@roleCheck(role= "admin")
def getParticipant():

    ucesnici = []

    participants = Participant.query.all()

    for participant in participants:
        ucesnik = {
            "id" : participant.id,
            "name" : participant.name,
            "individual" : participant.individual
            }
        ucesnici.append(ucesnik)


    return jsonify (participants = ucesnici), 200

def timeCheck(time):
    vreme = re.search('^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[01][0-9]):?[0-5][0-9])?$',time);
    if not vreme:
        return False
    return True


@application.route("/createElection", methods=["POST"])
@roleCheck(role = "admin")
def createElection():

    start = request.json.get("start")
    end = request.json.get("end")
    individual = request.json.get("individual","")
    participants = request.json.get("participants")

    if start is None or len(start) == 0:
        return jsonify(message = 'Field start is missing.'), 400
    if end is None or len(end) == 0:
        return jsonify(message = 'Field end is missing.'), 400
    if individual is None or len (str(individual)) == 0:
        return jsonify(message = "Field individual is missing."), 400
    if participants is None or len(str(participants)) == 0:
        return jsonify(message = "Field participants is missing."), 400

    if not timeCheck(start) or not timeCheck(end):
        return jsonify(message = "Invalid date and time."), 400

    startCheck = isodate.parse_datetime(start)
    endCheck = isodate.parse_datetime(end)

    elections = Election.query.all()

    if startCheck >= endCheck:
        return jsonify(message = "Invalid date and time."), 400

    for election in elections:

        if (startCheck.replace(tzinfo=None) <= election.start and endCheck.replace(tzinfo=None) > election.start) or \
                (startCheck.replace(tzinfo=None) >= election.start and startCheck.replace(tzinfo=None) < election.end):
            return jsonify(message = "Invalid date and time."), 400


    for participant in participants:
        part = Participant.query.filter(Participant.id == participant).first();
        if (not part or (part and part.individual != individual)):
            return jsonify(message = "Invalid participants."), 400

    if (len(participants) < 2):
        return jsonify(message = "Invalid participants."), 400

    izbor = Election(start=startCheck, end=endCheck, individual=individual)

    database.session.add(izbor)
    database.session.commit()

    brojac = 1
    lista = []

    for participant in participants:
        election_participant = ElectionParticipant(idParticipant=participant, idElection=izbor.id, pollNumbers=brojac,number_votes=0)
        lista.append(brojac)
        database.session.add(election_participant)
        database.session.commit()
        brojac = brojac + 1

    pollNmb = {
        'pollNumbers' : lista
    }

    return jsonify(pollNmb), 200

@application.route("/getElections", methods=["GET"])
@roleCheck(role="admin")
def getElections():

    elections = Election.query.all()

    lista = []

    for election in elections:
        el_part = ElectionParticipant.query.filter(ElectionParticipant.idElection == election.id).all()
        lista_p = []
        for ep in el_part:
            participant = Participant.query.filter(Participant.id == ep.idParticipant).first()
            lista_p.append({
                "id": participant.id,
                "name": participant.name
            })
        lista.append({
            "id": election.id,
            "start": str(election.start),
            "end": str(election.end),
            "individual": election.individual,
            "participants": lista_p
        })

    return jsonify({"elections" : lista}), 200

@application.route ( "/getResults", methods = ["GET"] )
@roleCheck ( role = "admin" )
def getResults():
    id =request.args.get("id")

    if (id == None):
        return jsonify(message ="Field id is missing."), 400

    election=Election.query.filter(Election.id == id).first()

    if ( id == "" or not election):
        return jsonify(message="Election does not exist."), 400

    if( election.end>datetime.datetime.now() and election.start<datetime.datetime.now()):
        return jsonify(message="Election is ongoing."), 400

    participant_elections = ElectionParticipant.query.filter(ElectionParticipant.idElection == election.id).all()

    total = 0
    for participant_election in participant_elections:
        total += participant_election.number_votes


    list = []


    if(election.individual==True):

        for participant_election in participant_elections:
            participant = Participant.query.filter(Participant.id == participant_election.idParticipant).first()

            if (total != 0):
                result = (participant_election.number_votes * 100) / total
            else:
                result = 0
            one = {
                "pollNumber": participant_election.pollNumbers,
                "name": participant.name,
                "result": round(result / 100, 2)
            }
            list.append(one)
    else:
        census_passed = []
        mandate_list = []
        quotient_list=[]

        #provera da li prelazi cenzus
        for participant_election in participant_elections:
            if (participant_election.number_votes >= total * 0.05):
                census_passed.append(participant_election)
                mandate_list.append(0)
                quotient_list.append(0)

        mandates=250
        while mandates>0:
            max=-1
            winner=-1
            for i in range(len(quotient_list)):
                quotient_list[i] = census_passed[i].number_votes/(mandate_list[i]+1)

            for i in range(len(quotient_list)):
                if(quotient_list[i] > max):

                    max = quotient_list[i]
                    winner = i


            mandate_list[winner] += 1

            mandates = mandates - 1

        for x in  range(len(census_passed)):
            participant = Participant.query.filter(Participant.id == census_passed[x].idParticipant).first()
            one = {
                    "name": participant.name,
                    "pollNumber": census_passed[x].pollNumbers,
                    "result": mandate_list[x]
            }
            list.append(one)


        for participant_election in participant_elections:
            if participant_election not in census_passed:

                participant = Participant.query.filter(Participant.id == participant_election.idParticipant).first()

                single = {
                    "name": participant.name,
                    "pollNumber": participant_election.pollNumbers,
                    "result": 0
                }
                list.append(single)

    list.sort(key=lambda x: x["pollNumber"])

    votes = []

    invalidvotes = InvalidVote.query.filter(InvalidVote.electionId == id).all()

    for invalidvote in invalidvotes:
        single = {
            "ballotGuid": invalidvote.ballotGuid,
            "electionOfficialJmbg": invalidvote.electionOfficialJMBG,
            "pollNumber": invalidvote.pollnumber,
            'reason': invalidvote.reason
        }
        votes.append(single)

    votes_total = {
        'participants': list,
        'invalidVotes': votes
    }

    return jsonify(votes_total), 200


if (__name__ == "__main__"):
    database.init_app(application)
    application.run(debug=True, host="0.0.0.0", port=5001)